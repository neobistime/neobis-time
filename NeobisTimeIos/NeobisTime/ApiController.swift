//
//  ApiController.swift
//  NeobisTime
//
//  Created by Islam on 7/21/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView

struct UrlPatterns {
    let users = "users/"
    let admin = "admin/"
    let auth = "auth/"
    let events = "events/"
    
    static let instance = UrlPatterns()
}

struct EndPoints {
    let userRegistration = "registration/"
    let departments = "departments/"
    let login = "login/"
    let auth = "jwt/create/"
    let refreshToken = "jwt/refresh/"
    let setPassword = "users/set_password/"
    let resetPassword = "send-email/"
    let userInfo = "info/"
    let logout = "token/logout/"
    let permission = "credentials/"
    let rooms = "all-rooms/"
    let roomEvents = "rooms/"
    let reserveRoom = "book-room/"
    let selfEventPost = "self-event/"
    let datesForCalendar = "all-dates/"
    let polls = "polls/"
    let selfEvents = "return-self-events/"
    static let instance = EndPoints()
}

class ApiController {
    
    static let instance = ApiController()
    
    func headerToRequest() -> HTTPHeaders {
        let credentials = DataManager.sharedInstance.getCredentials()
        print(credentials.accessToken)
        print(credentials.refreshToken)
        print(credentials.email)
        print(credentials.password)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(DataManager.sharedInstance.getCredentials().accessToken)",
            "Content-Type": "application/json"
        ]
        return headers
    }
    
    func dateDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    func dateDecoder2() -> JSONDecoder {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    func dateDecoderZone() -> JSONDecoder {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    
    
    func registerUser( user: UserRegistration, completion: @escaping (String)-> ()) {

        let urlString = ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.userRegistration).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = user.convertToParameters()
        AF.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.description.contains("users with this email already exists")){
                DispatchQueue.main.async {
                    completion("Пользователь с таким email уже существует")
                }
                return
            }
            if(response.response?.statusCode == 201){
                DispatchQueue.main.async {
                    completion("Регистрация прошла успешно. Чтобы использовать приложение вам понадобиться дать доступ для приложения на редактирование вашего Google календаря")
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова")
                }
            }
        }
    }
    
    func getDepartments(completion: @escaping ([Department])-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.departments).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url).validate().responseDecodable(of: [Department].self) { (response) in
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items)
            }
        }
    }
    
    func loginUser(user: UserLogin,completion: @escaping (String)-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.auth + EndPoints.instance.auth).getURLString()
        let urlString2 = ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.login).getURLString()
        guard let urlLogin = URL(string: urlString2) else{ return }
        guard let urlAuth = URL(string: urlString) else{ return }
        let parametrs = user.convertToParameters()
        AF.request(urlAuth, method: .post, parameters: parametrs, encoding: JSONEncoding.default).validate().responseDecodable(of: CredentialsToRequset.self){ (response) in
            guard let credentials = response.value else {
                if response.response?.statusCode == 401{
                    return DispatchQueue.main.async {
                        completion("Неправильные данные или заблокированный аккаунт")
                    }
                }else{
                    return DispatchQueue.main.async {
                        completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова")
                    }
                }
            }
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(credentials.access)",
                "Content-Type": "application/json"
            ]
            AF.request(urlLogin, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{ (response) in
                print(response)
                if(response.response?.statusCode == 200){
                    let credent = Credentials()
                    credent.accessToken = credentials.access
                    credent.refreshToken = credentials.refresh ?? ""
                    credent.email = user.email
                    credent.password = user.password
                    credent.credentialID = "Person"
                    DataManager.sharedInstance.addCredentials(object: credent)
                    DispatchQueue.main.async {
                        completion("success")
                    }
                }else{
                    DispatchQueue.main.async {
                        completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова")
                    }
                }
                
            }
        }
    }
    
    func logoutUser() {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.auth + EndPoints.instance.logout).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let headers: HTTPHeaders = headerToRequest()
        AF.request(url,method: .post,headers: headers ).responseJSON{ (response) in
            print(response)
        }
    }
    
    func refreshToken(completion: @escaping()->()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.auth + EndPoints.instance.refreshToken).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = ["refresh": DataManager.sharedInstance.getCredentials().refreshToken]
        AF.request(url, method: .post, parameters: parametrs ,encoding: JSONEncoding.default).validate().responseDecodable(of: CredentialsToRequset.self){ (response) in
            guard let credentials = response.value else {
                print(response)
                print(response.description)
                //print(response.response?.statusCode!)
                return
            }
            let credent = DataManager.sharedInstance.getCredentials()
            
            try! DataManager.sharedInstance.database.write {
                credent.accessToken = credentials.access
            }
            
        }
    }
    
    func getRooms(completion: @escaping ([Room],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.rooms).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: [Room].self) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [Room]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func getRoomEvents(room: Int,completion: @escaping ([RoomEvent],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.roomEvents + String(room) + "/").getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: RoomEventRequest.self) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [RoomEvent]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items.events, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func reserveRoom(reserve: ReserveRoom, completion: @escaping (String, Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.reserveRoom).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = reserve.convertToParameters()
        AF.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: headerToRequest()).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.response?.statusCode == 400){
                return DispatchQueue.main.async {
                    completion("Выберите другое время дня",response.response?.statusCode ?? 400)
                }
                
            }
            if(response.response?.statusCode == 201){
                DispatchQueue.main.async {
                    completion("Бронь создана успешно",response.response?.statusCode ?? 201)
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова",response.response?.statusCode ?? 0)
                }
            }
        }
    }
    
    func selfEventPost(create: SelfEventToPost, completion: @escaping (String, Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.selfEventPost).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = create.convertToParameters()
        AF.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: headerToRequest()).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.response?.statusCode == 400){
                return DispatchQueue.main.async {
                    completion("Выберите другое время дня", response.response?.statusCode ?? 400)
                }
                
            }
            if(response.response?.statusCode == 201){
                DispatchQueue.main.async {
                    completion("Ваше мероприятие создано", response.response?.statusCode ?? 201)
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова",response.response?.statusCode ?? 0)
                }
            }
        }
    }
    
    func changePassword(change: ChangePassword, completion: @escaping (String, Int)-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.auth + EndPoints.instance.setPassword).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = change.convertToParameters()
        AF.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: headerToRequest()).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.response?.statusCode == 400){
                return DispatchQueue.main.async {
                    completion("Что-то пошло не так. Попробуйте позже",response.response?.statusCode ?? 400)
                }
                
            }
            if(response.response?.statusCode == 204){
                DispatchQueue.main.async {
                    completion("Ваш пароль изменен.",response.response?.statusCode ?? 204)
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова",response.response?.statusCode ?? 0)
                }
            }
        }
    }
    
    func resetPassword(email: String, completion: @escaping (String)-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.resetPassword).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = ["email": email]
        AF.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: headerToRequest()).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.response?.statusCode == 400){
                return DispatchQueue.main.async {
                    completion("Что-то пошло не так. Попробуйте позже")
                }
                
            }
            if(response.response?.statusCode == 200){
                DispatchQueue.main.async {
                    completion("Новый пароль выслали вам на почту.")
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова")
                }
            }
        }
    }
    
    func getUserInfo(completion: @escaping (UserInfoDecode, Int)-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.userInfo).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: UserInfoDecode.self) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let item = UserInfoDecode()
                    DispatchQueue.main.async {
                        completion(item, 401)
                    }
                }
                return
            }
            guard let item = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(item,response.response?.statusCode ?? 200)
            }
        }
    }
    
    func getNotificationsEvents(completion: @escaping ([NotificationsEvent],Int)-> ()) {
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.events).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: [NotificationsEvent].self, decoder: dateDecoder2()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [NotificationsEvent]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func getDatesEventsCalendar(completion: @escaping ([DateCalendar],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.datesForCalendar).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: [DateCalendar].self, decoder: dateDecoder()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [DateCalendar]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func getPolls(completion: @escaping ([Poll],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.polls).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: [Poll].self, decoder: dateDecoder()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [Poll]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func getPollsDate(date: String,completion: @escaping ([Poll],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.polls).getURLString() + "?start_date=" + date
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: [Poll].self, decoder: dateDecoder()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [Poll]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func getPollInfo(id: String, completion: @escaping (Poll,Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.polls + id + "/").getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: Poll.self, decoder: dateDecoder()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let item = Poll()
                    DispatchQueue.main.async {
                        completion(item, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            DispatchQueue.main.async {
                completion(items, response.response?.statusCode ?? 0)
            }
        }
    }
    
    func pollVote(choise: Bool, id: String, completion: @escaping (String)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.polls + id + "/" + "vote/").getURLString()
        guard let url = URL(string: urlString) else{ return }
        let parametrs = ["choice": choise]
        AF.request(url, method: .patch, parameters: parametrs, encoding: JSONEncoding.default, headers: headerToRequest()).responseJSON{ (response) in
            print(response.description )
            print(response.response?.statusCode ?? "")
            if(response.response?.statusCode == 400){
                return DispatchQueue.main.async {
                    completion("Что-то пошло не так. Попробуйте позже")
                }
                
            }
            if(response.response?.statusCode == 202){
                DispatchQueue.main.async {
                    completion("Ваш ответ принят.")
                }
            }else{
                DispatchQueue.main.async {
                    completion("Что-то пошло не так. Проверьте интернет соединение и попробуйте снова")
                }
            }
        }
    }
    
    func getSelfEvents(completion: @escaping ([Poll],Int)-> ()) {
        let urlString = ApiAddress(endpoint: EndPoints.instance.selfEvents).getURLString()
        guard let url = URL(string: urlString) else{ return }
        AF.request(url,headers: headerToRequest()).validate().responseDecodable(of: Event.self, decoder: dateDecoderZone()) { (response) in
            if(response.response?.statusCode == 401){
                self.refreshToken{
                    let items = [Poll]()
                    DispatchQueue.main.async {
                        completion(items, 401)
                    }
                }
                return
            }
            guard let items = response.value else { return print(response.error ?? "") }
            var polls = [Poll]()
            for event in items.your_events{
                let poll = Poll(id: event.id, description: event.description, location: event.location, start: [event.start], summary: event.summary, choiseBool: true, owner: DataManager.sharedInstance.getUserInfo().email)
                polls.append(poll)
            }
            DispatchQueue.main.async {
                completion(polls, response.response?.statusCode ?? 0)
            }
        }
    }
}
