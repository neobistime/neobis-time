//
//  UITextView+.swift
//  NeobisTime
//
//  Created by Islam on 7/2/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        self.textContainerInset = UIEdgeInsets(top: 5, left: amount, bottom: 0, right: 0)
    }

}
