//
//  UIDatePicker+.swift
//  NeobisTime
//
//  Created by Islam on 8/4/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

extension UIDatePicker{
    
    func configureUI(cornerRadius: CGFloat, color: UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.backgroundColor = color
    }
    func configureUI(cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}
