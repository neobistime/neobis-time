//
//  UIButton+.swift
//  NeobisTime
//
//  Created by Islam on 6/25/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func standartBtnGreen() {
        self.layer.cornerRadius = 22
        self.setBackgroundColor(color: #colorLiteral(red: 0.2823529412, green: 0.7411764706, blue: 0.6588235294, alpha: 1) , forState: .normal)
    }
    
    func standartBtnColor(color: UIColor) {
        self.layer.cornerRadius = 22
        self.setBackgroundColor(color: color , forState: .normal)
    }
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        layer.masksToBounds = true
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    
}
