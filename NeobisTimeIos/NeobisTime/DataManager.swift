//
//  DataManager.swift
//  NeobisTime
//
//  Created by Islam on 7/29/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import RealmSwift

class DataManager {
    var   database:Realm
    static let  sharedInstance = DataManager()
    
    init() {
        database = try! Realm()
    }
    
    func getCredentials() ->  Credentials{
        let results: Credentials = database.object(ofType: Credentials.self, forPrimaryKey: "Person") ?? Credentials()
        return results
    }
    
    func deleteUser(){
        let user = getCredentials()
        try! database.write {
            database.delete(user)
        }
    }
    
    func getUserInfo() ->  UserInfo{
        let results: UserInfo = database.object(ofType: UserInfo.self, forPrimaryKey: "User") ?? UserInfo()
        return results
    }
    
    func addUserInfo(object: UserInfo)   {
        try! database.write {
            database.add(object, update: .all)
        }
    }
    func addCredentials(object: Credentials)   {
        try! database.write {
            database.add(object, update: .all)
        }
    }
    
    
}
