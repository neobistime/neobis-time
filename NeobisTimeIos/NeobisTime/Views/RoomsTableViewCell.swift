//
//  RoomsTableViewCell.swift
//  NeobisTime
//
//  Created by Islam on 7/6/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import UIKit

class RoomsTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    
    func setRoom(name: String, address: String) {
        nameLabel.text = name
        addressLabel.text = address
    }
}
