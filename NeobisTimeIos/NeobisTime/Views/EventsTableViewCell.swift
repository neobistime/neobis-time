//
//  EventsViewCellControllerTableViewCell.swift
//  NeobisTime
//
//  Created by Islam on 6/25/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    var configureDate = ConfigureDate()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setEvent(event: Poll) {
        let date = configureDate.formatDatePoll(date: event.start[0].dateTime)
        nameLabel.text = event.summary
        timeLabel.text = date
        addressLabel.text = event.location
        statusImage.image = statusImage.image?.withRenderingMode(.alwaysTemplate)
        if event.choice_bool == nil {
            statusImage.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            return
        }
        if event.choice_bool! {
            statusImage.tintColor = #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1)
        }else{
            statusImage.tintColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        if event.owner == DataManager.sharedInstance.getUserInfo().email {
            statusImage.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        }
    }

}
