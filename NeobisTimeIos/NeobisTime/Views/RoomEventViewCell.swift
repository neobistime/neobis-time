//
//  RoomEventsViewCell.swift
//  NeobisTime
//
//  Created by Islam on 7/6/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import UIKit

class RoomEventViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    
    
    func setEvent(dateStart: String, dateEnd: String, name: String, author: String) {
       // let dateStartNew = dateStart.replacingOccurrences(of: "T", with: " ")
        //let dateEndNew = dateEnd.replacingOccurrences(of: "T", with: " ")
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let formated = dateformatter.date(from: dateStart) ?? Date()
        dateformatter.dateFormat = "MMM d, yyyy"
        let date = dateformatter.string(from: formated)
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let formated1 = dateformatter.date(from: dateStart) ?? Date()
        let formated2 = dateformatter.date(from: dateEnd) ?? Date()
        dateformatter.dateFormat = "HH:mm"
        let date2 = dateformatter.string(from: formated1)
        let date3 = dateformatter.string(from: formated2)
        
        dateLabel.text = date
        timeLabel.text = date2 + "-" + date3
        nameLabel.text = name
        authorLabel.text = author
        layoutSubviews()
    }
    
    override func layoutSubviews() {

        cellView.layer.cornerRadius = 7.0
        cellView.layer.masksToBounds = true

        authorLabel.layer.cornerRadius = 7.0
        authorLabel.layer.masksToBounds = true

    }
    
}

