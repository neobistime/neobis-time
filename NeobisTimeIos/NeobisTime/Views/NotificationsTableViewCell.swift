//
//  NotificationsTableViewCell.swift
//  NeobisTime
//
//  Created by Islam on 7/9/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setEvent(status: String, name: String, date: String, address: String) {
        statusLabel.text = status
        nameLabel.text = name
        dateLabel.text = date
        addressLabel.text = address
    }

}
