//
//  CalendarEventViewCell.swift
//  NeobisTime
//
//  Created by Islam on 7/15/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import UIKit

class CalendarEventViewCell: UITableViewCell {
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var configureDate = ConfigureDate()
    func setEvent(name: String, time: Date, status: Bool?, owner: String) {
        nameLabel.text = name
        timeLabel.text = configureDate.formatDatePoll(date: time)
        if status == nil {
            statusImage.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            return
        }
        if status! {
            statusImage.tintColor = #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1)
        }else{
            statusImage.tintColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        if owner == DataManager.sharedInstance.getUserInfo().email {
            statusImage.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        }
    }

}
