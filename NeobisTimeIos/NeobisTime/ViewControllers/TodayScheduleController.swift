import UIKit
import NVActivityIndicatorView
class TodayScheduleController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var configureDate = ConfigureDate()
    @IBOutlet weak var tableView: UITableView!
    var events = [Poll]()
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        let date = configureDate.formatDateStringMonth(date: Date())
        self.title = date
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        loading = self.loadingView(view: self.view)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loading.startAnimating()
        ApiController.instance.getPollsDate(date: configureDate.formatDateString(date: Date()), completion: setPolls(polls:status:))
        ApiController.instance.getSelfEvents(completion: setSelfEvents(polls:status:))
    }
    func setPolls(polls: [Poll], status: Int) {
        if status == 401 {
            ApiController.instance.getPolls(completion: setPolls(polls:status:))
            tableView.reloadData()
        }
        events = polls
    }
    
    func setSelfEvents(polls: [Poll], status: Int) {
        if status == 401 {
            ApiController.instance.getSelfEvents(completion: setSelfEvents(polls:status:))
            tableView.reloadData()
        }
        let calendar = Calendar.current
        let selfEvents = polls.filter{calendar.dateComponents([.day], from: $0.start[0].dateTime) == calendar.dateComponents([.day], from: Date()) }
        for event in selfEvents{
            events.append(event)
        }
        tableView.reloadData()
        loading.stopAnimating()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayEventsViewCell", for: indexPath) as! DayEventsViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.setEvent(name: events[indexPath.row].summary, time: events[indexPath.row].start[0].dateTime, status: events[indexPath.row].choice_bool, owner: events[indexPath.row].owner)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEventDescriptionToday"{
            if let EventDetailController = segue.destination as? EventDetailController{
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell){
                    EventDetailController.id = ""
                    EventDetailController.event = events[indexPath.row]
                }
            }
        }
    }
}
