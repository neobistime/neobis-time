import UIKit
import PopupDialog
import NVActivityIndicatorView
class EventDetailController: UIViewController {
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var agreeStateLabel: UILabel!
    var configureDate = ConfigureDate()
    var popupController = PopupController()
    var id = ""
    var isAgree: Bool = true
    var event = Poll()
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
        acceptBtn.standartBtnGreen()
        rejectBtn.standartBtnColor(color: .systemRed)
        loading = self.loadingView(view: self.view)
        loading.startAnimating()
        if id.count == 0 {
            setEventDetails()
        }else{
            ApiController.instance.getPollInfo(id: id, completion: setEvent(event:status:))
        }
    }
    
    func setEvent(event: Poll, status: Int) {
        if status == 401 {
            ApiController.instance.getPollInfo(id: id, completion: setEvent(event:status:))
            setEventDetails()
        }
        self.event = event
        setEventDetails()
    }
    
    func setEventDetails() {
        self.title = event.summary
        if event.choice_bool != nil {
            if event.choice_bool! {
                isAgree = true
            }else{
                isAgree = false
            }
            changeBtnsForLabel()
        }else{
            acceptBtn.isHidden = false
            rejectBtn.isHidden = false
        }
        
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)]
        let dateTxt = NSMutableAttributedString(string:"Дата: ", attributes:attrs)
        let addressTxt = NSMutableAttributedString(string:"Адрес: ", attributes:attrs)
        let detailTxt = NSMutableAttributedString(string:"Описание: ", attributes:attrs)
        let statusTxt = NSMutableAttributedString(string:"Статус опроса до ", attributes:attrs)
        
        var array = [NSMutableAttributedString]()
        array = [dateTxt, addressTxt, detailTxt, statusTxt]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)]
        let dateDescTxt = NSMutableAttributedString(string:configureDate.formatDatePoll(date: event.start[0].dateTime) + "\n\n", attributes: attrs2)
        let addressDescTxt = NSMutableAttributedString(string:event.location + "\n\n", attributes: attrs2)
        let detailDescTxt = NSMutableAttributedString(string:event.description + "\n\n", attributes: attrs2)
        let statusDescTxt = NSMutableAttributedString(string:configureDate.formatDatePoll(date: event.start[0].dateTime) + "\n\n", attributes: attrs2)
        let event = NSMutableAttributedString()
        
        var array2 = [NSMutableAttributedString]()
        array2 = [dateDescTxt, addressDescTxt, detailDescTxt, statusDescTxt]
        
        for i in 0 ... 3{
            array[i].append(array2[i])
            event.append(array[i])
        }
        
        detailTextView.attributedText = event
        loading.stopAnimating()
    }
    @IBAction func acceptBtn(_ sender: Any) {
        isAgree = true
        changeBtnsForLabel()
        ApiController.instance.pollVote(choise: true, id: event.id, completion: answerPollVote(answer:))
    }
    @IBAction func cancelBtn(_ sender: Any) {
        isAgree = false
        changeBtnsForLabel()
        ApiController.instance.pollVote(choise: false, id: event.id, completion: answerPollVote(answer:))
    }
    
    func answerPollVote(answer: String) {
        let popup = popupController.createAlertPopup(title: answer, message: "", btnTxt: "Ok")
        self.present(popup, animated: true, completion: nil)
    }
    
    func changeBtnsForLabel() {
        agreeStateLabel.isHidden = false
        acceptBtn.isHidden = true
        rejectBtn.isHidden = true
        if !isAgree {
            agreeStateLabel.text = "Не пойду"
            agreeStateLabel.textColor = .systemRed
        }else{
            agreeStateLabel.text = "Пойду"
        }
    }
}
