import UIKit
import DropDown
import NVActivityIndicatorView
class EventsTableController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterDropdown: UIButton!
    
    let chooseFilterDropDown = DropDown()
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    var events = [Poll]()
    var filtered = [Poll]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        setupChooseDropDown()
        filterDropdown.imageEdgeInsets.left = 15
        filterDropdown.titleEdgeInsets.left = 20
        loading = self.loadingView(view: self.view)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loading.startAnimating()
        ApiController.instance.getPolls(completion: setPolls(polls:status:))
    }
    func setPolls(polls: [Poll], status: Int) {
        if status == 401 {
            ApiController.instance.getPolls(completion: setPolls(polls:status:))
            tableView.reloadData()
        }
        events = polls
        filtered = polls
        self.tableView.reloadData()
        loading.stopAnimating()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.setEvent(event: filtered[indexPath.row])
        return cell
    }
    
    @IBAction func choose(_ sender: AnyObject) {
        chooseFilterDropDown.show()
    }
    
    func setupChooseDropDown() {
        chooseFilterDropDown.anchorView = filterDropdown
        chooseFilterDropDown.bottomOffset = CGPoint(x: 0, y: filterDropdown.bounds.height)
        
        chooseFilterDropDown.dataSource = [
            "Пойду",
            "Не пойду",
            "Отсутствует ответ",
            "Все записи"
        ]
        
        chooseFilterDropDown.selectionAction = { [weak self] (index, item) in
            self?.filterDropdown.setTitle(item, for: .normal)
            switch item {
            case "Пойду":
                self!.filtered = self!.events.filter{ ($0.choice_bool == true)}
                self!.tableView.reloadData()
            case "Не пойду":
                self!.filtered = self!.events.filter{ ($0.choice_bool == false)}
                self!.tableView.reloadData()
            case "Отсутствует ответ":
                self!.filtered = self!.events.filter{ ($0.choice_bool == nil)}
                self!.tableView.reloadData()
            case "Все записи":
                self?.filtered = self!.events
                self!.tableView.reloadData()
            default:
                return
            }
        }
    }
    func filterEvents() {
        switch chooseFilterDropDown.selectedItem {
        case "Пойду":
            filtered = events.filter{ ($0.choice_bool == true)}
            tableView.reloadData()
        case "Не пойду":
            filtered = events.filter{ ($0.choice_bool == false)}
            tableView.reloadData()
        case "Отсутствует ответ":
            filtered = events.filter{ ($0.choice_bool == nil)}
            tableView.reloadData()
        case "Все записи":
            filtered = events
            tableView.reloadData()
        default:
            return
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEventDescription"{
            if let EventDetailController = segue.destination as? EventDetailController{
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell){
                    EventDetailController.id = ""
                    EventDetailController.event = filtered[indexPath.row]
                }
            }
        }
    }
}
