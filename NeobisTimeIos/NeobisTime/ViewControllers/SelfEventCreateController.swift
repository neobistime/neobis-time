import UIKit
import PopupDialog

class SelfEventCreateController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var nameInputView: UITextView!
    @IBOutlet weak var descriptionInputView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var popupController = PopupController()
    var configureDate = ConfigureDate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    func configureUI() {
        timePicker.configureUI(cornerRadius: 30, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        datePicker.configureUI(cornerRadius: 10, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        
        createBtn.standartBtnGreen()
        let allTextView = getTextView(view: self.view)
        for txtView in allTextView
        {
            txtView.setLeftPaddingPoints(5)
            txtView.delegate = self
        }
        
        nameInputView.text = "Название"
        nameInputView.textColor = UIColor.gray
        nameInputView.layer.cornerRadius = 5
        descriptionInputView.text = "Описание"
        descriptionInputView.textColor = UIColor.gray
        descriptionInputView.layer.cornerRadius = 5
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray || textView.text == "Название" || textView.textColor == UIColor.lightGray || textView.text == "Описание"{
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text!.isEmpty {
            if textView.accessibilityIdentifier == "descriptionInputView"{
                textView.text = "Описание"
            }else{
                textView.text = "Название"
            }
            textView.textColor = UIColor.lightGray
            return
        }
    }
    
    @IBAction func createEvent(_ sender: Any) {
        let name = nameInputView.text!
        let description = descriptionInputView.text!
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC+6")
        dateFormatter.dateFormat = "HH:mm"
        let inputDate = dateFormatter.string(from: timePicker.date)
        let choosedDate = configureDate.formatDate(date: datePicker.date)
        let currentDate = configureDate.formatDate(date: Date())
        let chooserdDateString = configureDate.formatDateString(date: choosedDate)
        if choosedDate < currentDate{
            let popup = popupController.createAlertPopup(title: "Выберите текущую или предстоящую дату", message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
            return
        }
        if name != "Название" && description != "Описание" {
            let selfEvent = SelfEventToPost(summary: name, description: description, start: chooserdDateString + " " + inputDate)
            ApiController.instance.selfEventPost(create: selfEvent, completion: answerSelfEvent(answer:status:))
        }else{
            let popup = popupController.createAlertPopup(title: "Заполните поля \"Название\" и \"Описание\"", message: "", btnTxt: "Ok")
            
            self.present(popup, animated: true, completion: nil)
        }
    }
    func answerSelfEvent(answer: String, status: Int) {
        let popup = popupController.createAlertPopup(title: answer, message: "", btnTxt: "Ok")
        self.present(popup, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}
