import UIKit
import ADDatePicker
import PopupDialog

class ReserveRoomController: UIViewController, UITextViewDelegate  {
    @IBOutlet weak var datePicker: ADDatePicker!
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var startTimePicker: UIDatePicker!
    @IBOutlet weak var finishTimePicker: UIDatePicker!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var descriptionInput: UITextView!
    
    var configureDate = ConfigureDate()
    var popupController = PopupController()
    var id = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        customDatePicker1()
        configureUI()
        
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
    }
    
    func configureUI() {
        
        lineView.layer.cornerRadius = 7.0
        lineView.layer.masksToBounds = true
        
        startTimePicker.configureUI(cornerRadius: 30, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        finishTimePicker.configureUI(cornerRadius: 30, color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        
        reserveBtn.standartBtnGreen()
        
        descriptionInput.text = "Цель бронирования"
        descriptionInput.delegate = self
        descriptionInput.textColor = UIColor.lightGray
        descriptionInput.layer.cornerRadius = 5
        descriptionInput.setLeftPaddingPoints(5)
    }
    
    func customDatePicker1(){
        var date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date)
        if(components.day == 31){
            var dayComponent    = DateComponents()
            dayComponent.day    = 1
            dayComponent.month  = 0
            let theCalendar     = Calendar.current
            date                = theCalendar.date(byAdding: dayComponent, to: Date())!
        }
        let currentYear = calendar.component(.year, from: date)
        datePicker.yearRange(inBetween: currentYear, end: currentYear + 1)
        datePicker.selectionType = .circle
        datePicker.bgColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        datePicker.deselectTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4534193065)
        datePicker.deselectedBgColor = .clear
        datePicker.selectedBgColor = .black
        datePicker.selectedTextColor = .black
        datePicker.layer.cornerRadius = 5
        datePicker.clipsToBounds = true
        datePicker.intialDate = Calendar.current.date(byAdding: .day, value: 0, to: date)!
        datePicker.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray || textView.text == "Цель бронирования"{
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text!.isEmpty {
            textView.text = "Цель бронирования"
            textView.textColor = UIColor.lightGray
            return
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func reserveRoom(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC+6")
        dateFormatter.dateFormat = "HH:mm"
        let inputDate = dateFormatter.string(from: startTimePicker.date)
        let inputDate2 = dateFormatter.string(from: finishTimePicker.date)
        let choosedDate = configureDate.formatDate(date: datePicker.getSelectedDate())
        let currentDate = configureDate.formatDate(date: Date())
        let description = descriptionInput.text!
        let chooserdDateString = configureDate.formatDateString(date: choosedDate)
        if choosedDate < currentDate{
            let popup = popupController.createAlertPopup(title: "Выберите текущую или предстоящую дату", message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
            return
        }
        if description != "Цель бронирования" {
            let room = ReserveRoom(title: description, dateStart: chooserdDateString + " " + inputDate, dateEnd: chooserdDateString + " " + inputDate2, room: String(id))
            ApiController.instance.reserveRoom(reserve: room, completion: answerReserveRoom(answer:status:))
        }else{
            let popup = popupController.createAlertPopup(title: "Заполните поле \"Цель бронирования\"", message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    func answerReserveRoom(answer: String, status: Int) {
        let popup = popupController.createAlertPopup(title: answer, message: "", btnTxt: "Ok")
        self.present(popup, animated: true, completion: nil)
    }
}

extension ReserveRoomController: ADDatePickerDelegate {
    func ADDatePicker(didChange date: Date) {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        print(dateformatter.string(from: date))
    }
}
