import Foundation
import DropDown
import UIKit
import PopupDialog
import SafariServices
import Navajo_Swift
class RegistrationController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var checkPasswordTextfield: UITextField!
    @IBOutlet weak var departmentDropdown: UIButton!
    @IBOutlet weak var registrationBtn: UIButton!
    @IBOutlet weak var passwordValidation: UILabel!
    
    let chooseDepartmentDropDown = DropDown()
    let popupController = PopupController()
    var departments = [Department]()
    private var validator = PasswordValidator.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        ApiController.instance.getDepartments(completion: setupChooseDropDown(departments:))
    }

    func configureUI() {
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
        
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            txtField.setLeftPaddingPoints(10)
            txtField.delegate = self
        }
        departmentDropdown.titleEdgeInsets.left = 15
        registrationBtn.standartBtnGreen()
    }
    
    @IBAction func choose(_ sender: AnyObject) {
        chooseDepartmentDropDown.show()
    }
    
    func setupChooseDropDown(departments: [Department]) {
        chooseDepartmentDropDown.anchorView = departmentDropdown
        chooseDepartmentDropDown.bottomOffset = CGPoint(x: 0, y: departmentDropdown.bounds.height)
        self.departments = departments
        var departaments = [String]()
        for item in departments{
            departaments.append(item.name)
        }
        
        chooseDepartmentDropDown.dataSource = departaments
        
        
        chooseDepartmentDropDown.selectionAction = { [weak self] (index, item) in
            self?.departmentDropdown.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func registrateUser(_ sender: Any) {
        let name = nameTextField.text!
        let email = emailTextField.text!
        let password = passwordTextField.text!
        let checkPassword = checkPasswordTextfield.text!
        let department = chooseDepartmentDropDown.selectedItem
        
        if !name.isEmpty && !email.isEmpty && !password.isEmpty && !checkPassword.isEmpty && department != nil{
            if password != checkPassword {
                let popup = popupController.createAlertPopup(title: "Пароли не совпадают", message: "", btnTxt: "Ok")
                self.present(popup, animated: true, completion: nil)
                return
            }
            var departmentId = 0
            for item in departments{
                if item.name == chooseDepartmentDropDown.selectedItem{
                    departmentId = item.id
                }
            }
            let user = UserRegistration(name: name, email: email, password: password, department: departmentId)
            ApiController.instance.registerUser(user: user, completion: successRegistrationPopup(answer:))
        }else{
            let popup = popupController.createAlertPopup(title: "Заполните все поля", message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    func successRegistrationPopup(answer: String) {
        let btn = CancelButton(title: "Ok") {
            guard let url = URL(string: ApiAddress(endpoint: UrlPatterns.instance.users + EndPoints.instance.permission).getURLString()) else{ return}
            let vc = SFSafariViewController(url: url)
            vc.delegate = self as? SFSafariViewControllerDelegate
            self.present(vc, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
        let popup = popupController.createAlertPopupWithBtn(title: answer, message: "", btn: btn)
        self.present(popup, animated: true, completion: nil)
    }
    @IBAction func checkPasswordValid(_ sender: Any) {
        validatePassword()
    }
    private func validatePassword() {
        passwordValidation.numberOfLines = 0;
        let lengthRule = LengthRule(min: 8, max: 24)
        let uppercaseRule = RequiredCharacterRule(preset: .lowercaseCharacter)
        
        validator = PasswordValidator(rules: [lengthRule, uppercaseRule])
        let password = passwordTextField.text ?? ""
        
        if let failingRules = validator.validate(password) {
            passwordValidation.textColor = .red
            passwordValidation.text = failingRules.map { return $0.localizedErrorDescription }.joined(separator: "\n")
            registrationBtn.isEnabled = false
        } else {
            passwordValidation.textColor = .green
            passwordValidation.text = " "
            registrationBtn.isEnabled = true
        }
    }
}
extension RegistrationController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
