import UIKit
import PopupDialog
import NVActivityIndicatorView
class LoginController: UIViewController {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var toRegistrationBtn: UIButton!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var restorePasswordBtn: UIButton!
    
    var popupController = PopupController()
    var iconClick = true
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    var restorePassword: Bool = true{
        willSet{
            if newValue{
                self.navigationController?.title = "Авторизация"
                passwordTextField.isHidden = false
                toRegistrationBtn.isHidden = false
                restorePasswordBtn.titleLabel?.text = "Забыли пароль?"
                loginBtn.setTitle("Войти", for: .normal)
            }else{
                self.navigationController?.title = "Восстановление"
                passwordTextField.isHidden = true
                toRegistrationBtn.isHidden = true
                restorePasswordBtn.titleLabel?.text = "Войти"
                loginBtn.setTitle("Восстановить", for: .normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        if DataManager.sharedInstance.getCredentials().password.count != 0 {
            let data = DataManager.sharedInstance.getCredentials()
            let user = UserLogin(email: data.email, password: data.password)
            loading = self.loadingView(view: self.view)
            loading.startAnimating()
            ApiController.instance.loginUser(user: user, completion: moveToMain(answer:))
            
        }
    }
    
    func configureUI() {
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
        
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            txtField.setLeftPaddingPoints(10)
            txtField.delegate = self
        }
        loginBtn.standartBtnColor(color: #colorLiteral(red: 0.3843137255, green: 0.007843137255, blue: 0.9333333333, alpha: 1))
        toRegistrationBtn.standartBtnGreen()
        
        passwordTextField.rightViewMode = .unlessEditing
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "eye"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -24, bottom: 0, right: 15)
        button.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.btnPasswordVisiblityClicked), for: .touchUpInside)
        passwordTextField.rightView = button
        passwordTextField.rightViewMode = .always
    }
    @IBAction func switchLogin(_ sender: Any) {
        restorePassword = !restorePassword
    }
    @IBAction func btnPasswordVisiblityClicked(_ sender: Any) {
        if(iconClick == true) {
            passwordTextField.isSecureTextEntry = false
        } else {
            passwordTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @IBAction func loginUser(_ sender: Any) {
        if restorePassword {
            let email = emailTxtField.text!
            let password = passwordTextField.text!
            if !email.isEmpty && !password.isEmpty {
                let user = UserLogin(email: email, password: password)
                
                ApiController.instance.loginUser(user: user, completion:moveToMain(answer:))
            }else{
                let popup = popupController.createAlertPopup(title: "Заполните поля \"Электронный адрес\" и \"Пароль\"", message: "", btnTxt: "Ok")
                self.present(popup, animated: true, completion: nil)
            }
        }else{
            let email = emailTxtField.text!
            if !email.isEmpty{
                ApiController.instance.resetPassword(email: email, completion: answerResetPassword(answer:))
            }else{
                let popup = popupController.createAlertPopup(title: "Заполните поля \"Электронный адрес\"", message: "", btnTxt: "Ok")
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    func answerResetPassword(answer: String) {
        let btn = CancelButton(title: "Ok") {
            
            self.iconClick = !self.iconClick
        }
        let popup = popupController.createAlertPopupWithBtn(title: answer, message: "", btn: btn)
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func moveToMain(answer: String) {
        loading.stopAnimating()
        if answer == "success" {
            performSegue(withIdentifier: "toMain", sender: nil)
        }else{
            let popup = popupController.createAlertPopup(title: answer, message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
        }
    }
}

extension LoginController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
