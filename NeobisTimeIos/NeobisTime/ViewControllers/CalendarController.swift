import UIKit
import FSCalendar
import NVActivityIndicatorView
class CalendarController: UIViewController, UITableViewDataSource, UITableViewDelegate, FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var calendarEventTable: UITableView!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    var configureDate = ConfigureDate()
    var datesWithEvent = [String]()
    var datesWithMultipleEvents = [String]()
    var events = [Poll]()
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView, action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        calendarView.calendarHeaderView.backgroundColor = #colorLiteral(red: 0.3764705882, green: 0.4549019608, blue: 0.9843137255, alpha: 1)
        self.calendarView.select(Date())
        self.view.addGestureRecognizer(self.scopeGesture)
        self.calendarEventTable.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendarView.scope = .month
        self.calendarView.accessibilityIdentifier = "calendar"
        loading = self.loadingView(view: self.view)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loading.startAnimating()
        ApiController.instance.getDatesEventsCalendar(completion: setEventsToCalendar(events:status:))
        ApiController.instance.getPollsDate(date: configureDate.formatDateString(date: Date()), completion: setPolls(polls:status:))
        ApiController.instance.getSelfEvents(completion: setSelfEvents(polls:status:))
    }
    func setPolls(polls: [Poll], status: Int) {
        if status == 401 {
            ApiController.instance.getPollsDate(date: configureDate.formatDateString(date: Date()), completion: setPolls(polls:status:))
            
        }
        events = polls
    }
    
    func setSelfEvents(polls: [Poll], status: Int) {
        if status == 401 {
            ApiController.instance.getSelfEvents(completion: setSelfEvents(polls:status:))
           // calendarEventTable.reloadData()
        }
        let calendar = Calendar.current
        let selfEvents = polls.filter{calendar.dateComponents([.day], from: $0.start[0].dateTime) == calendar.dateComponents([.day], from: calendarView.selectedDate!) }
        for event in selfEvents{
            events.append(event)
        }
        calendarEventTable.reloadData()
        loading.stopAnimating()
    }
    func setEventsToCalendar(events: [DateCalendar], status: Int) {
        if status == 401 {
            ApiController.instance.getDatesEventsCalendar(completion: setEventsToCalendar(events:status:))
            calendarView.reloadData()
        }
        if status != 200 {
            return
        }
        let result = configureDate.formatStringToCalendar(dates: events)
        datesWithEvent = result.0
        datesWithMultipleEvents = result.1
        calendarView.reloadData()
    }
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let dateString = configureDate.formatDateString(date: date)
        
        if self.datesWithEvent.contains(dateString) {
            return 1
        }
        
        if self.datesWithMultipleEvents.contains(dateString) {
            return 3
        }
        
        return 0
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.calendarEventTable.contentOffset.y <= -self.calendarEventTable.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            @unknown default:
                print ("error")
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        loading.startAnimating()
        ApiController.instance.getPollsDate(date: configureDate.formatDateString(date: date), completion: setPolls(polls:status:))
        ApiController.instance.getSelfEvents(completion: setSelfEvents(polls:status:))
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarEventViewCell", for: indexPath) as! CalendarEventViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.setEvent(name: events[indexPath.row].summary, time: events[indexPath.row].start[0].dateTime, status: events[indexPath.row].choice_bool, owner: events[indexPath.row].owner)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEventDescriptionCalendar"{
            if let EventDetailController = segue.destination as? EventDetailController{
                if let cell = sender as? UITableViewCell, let indexPath = calendarEventTable.indexPath(for: cell){
                        EventDetailController.id = ""
                        EventDetailController.event = events[indexPath.row]
                }
            }
        }
    }
}
