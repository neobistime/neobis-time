import UIKit

class RoomsTableController: UITableViewController {
    var rooms: [Room] = []
    @IBOutlet var roomTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiController.instance.getRooms(completion: setRooms(_:status:))
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
    }
    
    func setRooms(_ roomsList: [Room]?, status: Int) {
        if status == 401 {
            ApiController.instance.getRooms(completion: setRooms(_:status:))
            tableView.reloadData()
        }
        guard let list = roomsList  else {
            return
        }
        rooms = list
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoomsTableViewCell", for: indexPath) as! RoomsTableViewCell
        let item: Room
        item = rooms[indexPath.row]
        cell.setRoom(name: item.name, address: item.address)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRoomEventsSegue"{
            if let RoomEventsController = segue.destination as? RoomEventsController{
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell){
                    RoomEventsController.id = rooms[indexPath.row].id
                }
            }
        }
    }
}
