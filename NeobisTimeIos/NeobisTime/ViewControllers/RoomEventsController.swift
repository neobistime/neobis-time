import UIKit
import NVActivityIndicatorView

class RoomEventsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var noEventsLabel: UILabel!
    private let reuseIdentifier = "RoomEventViewCell"
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    var events: [RoomEvent] = []
    var id = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
        reserveBtn.standartBtnGreen()
        loading = self.loadingView(view: self.view)
        loading.startAnimating()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ApiController.instance.getRoomEvents(room: id,completion: setRoomEvents(_:status:))
    }
    func setRoomEvents(_ eventsList: [RoomEvent]?, status: Int) {
        if status == 401 {
            ApiController.instance.getRoomEvents(room: id,completion: setRoomEvents(_:status:))
            collectionView.reloadData()
        }
        guard let list = eventsList  else {
            return
        }
        if list.count == 0 {
            noEventsLabel.isHidden = false
        }
        events = list
        self.collectionView.reloadData()
        loading.stopAnimating()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RoomEventViewCell
        let item: RoomEvent
        item = events[indexPath.row]
        cell.setEvent(dateStart: item.date_start, dateEnd: item.date_end, name: item.title, author: item.owner)
        
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toReserveRoom"{
            if let ReserveRoomController = segue.destination as? ReserveRoomController{
                ReserveRoomController.id = id
            }
        }
    }
}
extension RoomEventsController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.size.width
        return CGSize(width: width, height: 70)
    }
}
