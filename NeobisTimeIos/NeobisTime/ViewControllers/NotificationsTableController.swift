import UIKit
import DropDown
import SafariServices
import NVActivityIndicatorView

class NotificationsTableController:  UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterDropdown: UIButton!
    var items = [NotificationsEvent]()
    var filtered = [NotificationsEvent]()
    let configureDate = ConfigureDate()
    let chooseFilterDropDown = DropDown()
    var loading = NVActivityIndicatorView(frame: .zero, type: .squareSpin, color: #colorLiteral(red: 0.1607843137, green: 0.7019607843, blue: 0.6039215686, alpha: 1), padding: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        configureDropdown()
        loading = self.loadingView(view: self.view)
        ApiController.instance.getUserInfo(completion: setUserInfo(userInfo:status:))
        loading.startAnimating()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ApiController.instance.getNotificationsEvents(completion: setNotificationEvents(events:status:))
    }
    func setUserInfo(userInfo: UserInfoDecode, status: Int) {
        if status == 401 {
            ApiController.instance.getUserInfo(completion: setUserInfo(userInfo:status:))
            return
        }
        let user = UserInfo()
        user.name = userInfo.name
        user.department = userInfo.department
        user.email = userInfo.email
        user.credentialID = "User"
        user.isStaff = userInfo.is_staff
        DataManager.sharedInstance.addUserInfo(object: user)
        
    }
    
    func setNotificationEvents(events: [NotificationsEvent], status: Int) {
        if status == 401 {
            ApiController.instance.getNotificationsEvents(completion: setNotificationEvents(events:status:))
            return
        }
        items = events
        filtered = events
        tableView.reloadData()
        loading.stopAnimating()
    }
    
    func configureDropdown(){
        setupChooseDropDown()
        let origImage = UIImage(named: "dropdownArrow")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        filterDropdown.setImage(tintedImage, for: .normal)
        filterDropdown.tintColor = .black
        filterDropdown.imageEdgeInsets.left = 15
        filterDropdown.titleEdgeInsets.left = 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        
        cell.setEvent(status: filtered[indexPath.row].status_id, name: items[indexPath.row].summary, date: configureDate.formatDateString(date: items[indexPath.row].start), address: items[indexPath.row].location)
        
        return cell
    }
    
    
    
    @IBAction func choose(_ sender: AnyObject) {
        chooseFilterDropDown.show()
    }
    
    func setupChooseDropDown() {
        chooseFilterDropDown.anchorView = filterDropdown
        chooseFilterDropDown.bottomOffset = CGPoint(x: 0, y: filterDropdown.bounds.height)
        
        chooseFilterDropDown.dataSource = [
            "Добавлено",
            "Изменено",
            "Удалено",
            "Все записи"
        ]
        
        chooseFilterDropDown.selectionAction = { [weak self] (index, item) in
            self?.filterDropdown.setTitle(item, for: .normal)
            switch item {
            case "Добавлено":
                self!.filtered = self!.items.filter{ ($0.status_id == "Добавлено")}
                self!.tableView.reloadData()
            case "Изменено":
                self!.filtered = self!.items.filter{ ($0.status_id == "Изменено")}
                self!.tableView.reloadData()
            case "Удалено":
                self!.filtered = self!.items.filter{ ($0.status_id == "Удалено")}
                self!.tableView.reloadData()
            case "Все записи":
                self?.filtered = self!.items
                self!.tableView.reloadData()
            default:
                return
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEventDescriptionNotifications"{
            if let EventDetailController = segue.destination as? EventDetailController{
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell){
                    EventDetailController.id = filtered[indexPath.row].id
                }
            }
        }
    }
}
