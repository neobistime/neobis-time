import Foundation
import UIKit

class ConfigureDate {
    
    func formatDate(date: Date) -> Date {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let day = dateformatter.string(from: date)
        guard let formatedDate = dateformatter.date(from: day) else { return Date() }
        
        return formatedDate
    }
    
    func formatDateString(date: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let day = dateformatter.string(from: date)
        return day
    }
    
    func formatDatePoll(date: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MMMM HH:mm"
        dateformatter.locale = Locale(identifier: "ru_RU")
        let day = dateformatter.string(from: date)
        return day
    }
    
    func formatDateStringMonth(date: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MMMM"
        dateformatter.locale = Locale(identifier: "ru_RU")
        let day = dateformatter.string(from: date)
        return day
    }
    
    func formatString(date: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let day = dateformatter.date(from: date) ?? Date()
        dateformatter.dateFormat = "dd-MMMM HH:mm"
        dateformatter.locale = Locale(identifier: "ru_RU")
        let day2 = dateformatter.string(from: day)
        return day2
    }
    func formatStringRemoveLast(date: String) -> String {
        var dat = date
        dat.removeLast(5)
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let day = dateformatter.date(from: dat) ?? Date()
        dateformatter.dateFormat = "dd-MMMM HH:mm"
        dateformatter.locale = Locale(identifier: "ru_RU")
        let day2 = dateformatter.string(from: day)
        return day2
    }
    
    func formatStringToCalendar(dates: [DateCalendar]) -> ([String], [String]) {
        var days = [String]()
        var many = [String]()
        let dateformatter = DateFormatter()
        let dateformatter2 = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateformatter2.dateFormat = "yyyy-MM-dd"
        for day in dates{
            let d = dateformatter2.string(from: day.start.dateTime)
            days.append(d)
        }
        let manyEventsDay = Dictionary(grouping: days, by: {$0}).filter { $1.count > 1 }.keys
        for event in manyEventsDay{
            many.append(event)
        }
        for event in many{
             days.removeAll{$0 == event}
        }
        print(manyEventsDay)
        return (days,many)
    }
}
