import Foundation
import UIKit
import WebKit
class AdminPanelController: UIViewController {
    var webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        let source = "document.getElementById('id_username').value = '\(DataManager.sharedInstance.getCredentials().email)' ;" + "document.getElementById('id_password').value = '\(DataManager.sharedInstance.getCredentials().password)' ;" + "document.getElementById('original_name').click() ;"
        let userScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)

        let userContentController = WKUserContentController()
        userContentController.addUserScript(userScript)

        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
        webView = WKWebView(frame: self.view.bounds, configuration: configuration)
        let urlString = ApiAddress(endpoint: UrlPatterns.instance.admin).getURLString()
        guard let url = URL(string: urlString) else{ return }
        let request = URLRequest(url: url)
        self.view = webView
        webView.load(request)
        
    }
//    override func loadView() {
//        self.view = webView
//    }
}
