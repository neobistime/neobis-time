import UIKit
import Navajo_Swift
import PopupDialog

class ChangePasswordController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmNewPasswordField: UITextField!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var passwordValidation: UILabel!
    var popupController = PopupController()
    private var validator = PasswordValidator.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: false)
        
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            txtField.setLeftPaddingPoints(10)
            txtField.delegate = self
        }
        changePasswordBtn.standartBtnGreen()
    }
    
    @IBAction func changePassword(_ sender: Any) {
        let password = oldPasswordField.text!
        let newPassword = newPasswordField.text!
        let confirmNewPassword = confirmNewPasswordField.text!
        if !password.isEmpty && !newPassword.isEmpty && !confirmNewPassword.isEmpty{
            if newPassword != confirmNewPassword {
                let popup = popupController.createAlertPopup(title: "Пароли не совпадают", message: "", btnTxt: "Ok")
                self.present(popup, animated: true, completion: nil)
                return
            }else{
                let change = ChangePassword(password: password, newPassword: newPassword)
                ApiController.instance.changePassword(change: change, completion: answerChangePassword(answer:status:))
            }
        }else{
            let popup = popupController.createAlertPopup(title: "Заполните все поля", message: "", btnTxt: "Ok")
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    func answerChangePassword(answer: String, status: Int) {
        let btn = CancelButton(title: "Ok") {
            self.navigationController?.popViewController(animated: true)
        }
        let popup = popupController.createAlertPopupWithBtn(title: answer, message: "", btn: btn)
        self.present(popup, animated: true, completion: nil)
    }
    
    @IBAction func checkPassword(_ sender: Any) {
        validatePassword()
    }
    private func validatePassword() {
        passwordValidation.numberOfLines = 0;
        let lengthRule = LengthRule(min: 8, max: 24)
        let uppercaseRule = RequiredCharacterRule(preset: .lowercaseCharacter)
        
        validator = PasswordValidator(rules: [lengthRule, uppercaseRule])
        let password = newPasswordField.text ?? ""
        
        if let failingRules = validator.validate(password) {
            passwordValidation.textColor = .red
            passwordValidation.text = failingRules.map { return $0.localizedErrorDescription }.joined(separator: "\n")
            changePasswordBtn.isEnabled = false
        } else {
            passwordValidation.textColor = .green
            passwordValidation.text = " "
            changePasswordBtn.isEnabled = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

