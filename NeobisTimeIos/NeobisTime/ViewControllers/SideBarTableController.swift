import Foundation
import UIKit
import PopupDialog


class SideBarTableController: UITableViewController {
    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var userInfoLabel: UITextView!
    @IBOutlet weak var adminPanelCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        createExitBtn()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.toPersonalInformation))
        personalInfoView.addGestureRecognizer(gesture)
        adminPanelCell.isHidden = true
        if DataManager.sharedInstance.getUserInfo().isStaff {
            adminPanelCell.isHidden = false
        }
        let user = DataManager.sharedInstance.getUserInfo()
        userInfoLabel.text = user.name + "\n" + user.department + "\n" + user.email + "\n"
    }
    
    @objc func toPersonalInformation(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "toPersonalInformation", sender: nil)
    }
    
    func createExitBtn() {
        let button = UIButton.init(frame: CGRect(origin: CGPoint(x: 0, y: self.view.frame.size.height - 50), size: CGSize(width: self.view.frame.width, height: 50)))
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 17, bottom: 20, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 0, right: 0);
        button.backgroundColor = UIColor.clear
        button.setImage(UIImage(named: "exit"), for: .normal)
        button.setTitle("Выход", for: .normal)
        button.titleLabel?.font = UIFont(name: "Apple SD Gothic Neo", size: 18)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(self.exitBtn(_:)), for: .touchUpInside)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.navigationController?.view.addSubview(button)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    @IBAction func exitBtn(_ sender: Any){
        DestructiveButton.appearance().titleColor = .red
        let title = "Вы уверены что хотите выйти из аккаунта?"
        let message = ""
        let popup = PopupDialog(title: title, message: message)
        
        let buttonOne = CancelButton(title: "Отмена") {
            print("Cancel")
        }
        let buttonTwo = DestructiveButton(title: "Выйти") {
            print("Exit")
            DataManager.sharedInstance.deleteUser()
            ApiController.instance.logoutUser()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Authorization", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
            let navController = UINavigationController(rootViewController: nextViewController)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated:true, completion:nil)
        }
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
    }
}

