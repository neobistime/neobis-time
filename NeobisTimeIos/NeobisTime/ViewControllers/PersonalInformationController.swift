import UIKit

class PersonalInformationController: UIViewController {
    @IBOutlet weak var toChangePasswBtn: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var emailLabel: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar(navBar: self.navigationController!, navItem: self.navigationItem, hasSideMenu: true)
        toChangePasswBtn.standartBtnGreen()
        setUserInfo()
    }
    func setUserInfo() {
        let user = DataManager.sharedInstance.getUserInfo()
        nameLabel.text = "Привет, " + user.name
        departmentLabel.text = user.department
        if user.department.count == 0{
            departmentLabel.text = "iOS"
        }
        
        emailLabel.text = user.email
    }
}
