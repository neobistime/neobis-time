//
//  ApiAddress.swift
//  NeobisTime
//
//  Created by Islam on 7/21/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import Alamofire

struct ApiAddress {
    var scheme = "https"
    var domain = "neobis-time.herokuapp.com"
    //var apiKey = "0000"
    var endpoint: String = ""
    var param: String = ""
    
    init(endpoint: String) {
        self.endpoint = endpoint
    //    self.param = getStringFrom(parameter: nil)
    }
    
    init(endpoint: String, param: [String: Any]?) {
        self.endpoint = endpoint
      //  self.param = getStringFrom(parameter: param as! [String: String]?)
    }
    
    func getURLString() -> String {
        return "\(scheme)://\(domain)/\(endpoint)\(param)"
    }
    
    func getURL() -> URL? {
        return URL(string: getURLString())
    }
    
//    func getStringFrom(parameter: [String:String]?) -> String {
//        var parameterString = "&api_key=\(apiKey)"
//
//        if parameter == nil {
//            return parameterString
//        }
//        for (key, value) in parameter! {
//            if key.contains("readyStringParameter") {
//                parameterString += value
//            } else {
//                parameterString += "&\(key)=\(value)"
//            }
//        }
//        return parameterString
//    }
}
