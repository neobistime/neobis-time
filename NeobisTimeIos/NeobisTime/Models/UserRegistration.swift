import Foundation

class UserRegistration: Codable {
    let name: String
    let email: String
    let password: String
    let departament: Int
    
    func convertToParameters() -> [String : String] {
        return ["name": name, "email": email, "password": password, "departament_id": String(departament)]
    }
    
    init(name: String, email: String, password: String, department: Int) {
        self.name = name
        self.email = email
        self.password = password
        self.departament = department
    }
}
