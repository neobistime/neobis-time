//
//  Credentials.swift
//  NeobisTime
//
//  Created by Islam on 7/29/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation
import RealmSwift

class Credentials: Object {
    @objc dynamic var credentialID = UUID().uuidString
    @objc dynamic var email: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var accessToken: String = ""
    @objc dynamic var refreshToken: String = ""
    
    override static func primaryKey() -> String? {
      return "credentialID"
    }
}
