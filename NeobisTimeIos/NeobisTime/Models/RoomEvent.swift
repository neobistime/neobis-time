import Foundation

class RoomEvent: Codable {
    let title: String
    let date_start: String
    let date_end: String
    let owner: String
    
    init(title: String) {
        self.title = title
        self.date_start = ""
        self.date_end = ""
        self.owner = ""
    }
}
