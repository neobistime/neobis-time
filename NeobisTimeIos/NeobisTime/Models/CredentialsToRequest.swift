//
//  CredentialsToRequest.swift
//  NeobisTime
//
//  Created by Islam on 7/29/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

class CredentialsToRequset: Codable {

    let access: String
    let refresh: String?
    
    func convertToParametersRefresh() -> [String : String] {
        return ["refresh": refresh ?? ""]
    }

}
