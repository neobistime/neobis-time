import Foundation
class ReserveRoom: Codable {
    let title: String
    let dateStart: String
    let dateEnd: String
    let room: String

    func convertToParameters() -> [String : String] {
        return ["title": title, "date_start": dateStart, "date_end": dateEnd, "room": room]
    }

    init(title: String, dateStart: String, dateEnd: String, room: String) {
        self.title = title
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.room = room
    }
}

