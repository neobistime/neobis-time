import Foundation
import RealmSwift

class UserInfoDecode: Codable {
    let name: String
    let email: String
    let department: String
    let is_staff: Bool
    
    func convertToParameters() -> [String : String] {
        return ["name": name, "email": email,"department": department]
    }
    
    
    init() {
        self.name = ""
        self.email = ""
        self.department = ""
        self.is_staff = false
    }
}

class UserInfo: Object {
    @objc dynamic var credentialID = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var department: String = ""
    @objc dynamic var isStaff: Bool = false
    
    override static func primaryKey() -> String? {
        return "credentialID"
    }
}
