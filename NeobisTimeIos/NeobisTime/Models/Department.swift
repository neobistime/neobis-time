//
//  Departments.swift
//  NeobisTime
//
//  Created by Islam on 7/27/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

class Department: Codable {
    let id: Int
    let name: String
}
