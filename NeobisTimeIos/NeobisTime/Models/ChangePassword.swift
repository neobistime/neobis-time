import Foundation

class ChangePassword: Codable {
    let password: String
    let newPassword: String
    
    func convertToParameters() -> [String : String] {
        return ["current_password": password, "new_password": newPassword]
    }
    
    init(password: String, newPassword: String) {
        self.password = password
        self.newPassword = newPassword
    }
}
