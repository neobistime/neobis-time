import Foundation

class NotificationsEvent: Codable {
    let id: String
    let summary: String
    let location: String
    let description: String
    let status_id: String
    let start: Date
    
    init(id: String, summary: String, location: String, description: String, status_id: String, start: Date) {
        self.id = id
        self.summary = summary
        self.location = location
        self.description = description
        self.status_id = status_id
        self.start = start
    }
    init() {
        self.id = ""
        self.summary = ""
        self.location = ""
        self.description = ""
        self.status_id = ""
        self.start = Date()
    }
}
