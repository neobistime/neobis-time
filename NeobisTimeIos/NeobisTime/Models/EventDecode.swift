import Foundation

class Event: Codable {
    let your_events: [EventItem]
}


class EventItem: Codable {
    let id: String
    let created: String
    let description: String
    let location: String
    let start: DatesDecode
    let summary: String
    let updated: String
    
    init(id: String, created: String, description: String, location: String, start: DatesDecode, summary: String, updated: String) {
        self.created = created
        self.description = description
        self.location = location
        self.start = start
        self.summary = summary
        self.updated = updated
        self.id = id
    }
}


class DatesDecode: Codable {
    let dateTime: Date
    let timeZone: String
    
    init(dateTime: Date, timeZone: String) {
        self.dateTime = dateTime
        self.timeZone = timeZone
    }
}

class Reminders: Codable {
    let useDefault: Bool
}
