//
//  DatesToSelfEvent.swift
//  NeobisTime
//
//  Created by Islam on 8/6/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

class DatesToSelfEvent: Codable {
    let dates : [DateInfo]
//    init(dateInfo: DateInfo) {
//        self.dates = [DateInfo]()
//        dates.append(dateInfo)
//    }
}

class DateInfo: Codable {
    let dateTime: String
    let timeZone = "Asia/Bishkek"
    
    init(date: String) {
        self.dateTime = date
    }
}
