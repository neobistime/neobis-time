import Foundation

class Room: Codable {
    let id: Int
    let name: String
    let address: String
}
