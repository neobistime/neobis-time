import Foundation

class DateCalendar: Codable {
    let summary: String
    let start: DatesDecode
    
    init(summary: String, start: DatesDecode) {
        self.start = start
        self.summary = summary
    }
    //    init() {
    //        self.start = [DatesDecode]()
    //    }
}
