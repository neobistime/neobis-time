//
//  RoomEventRequest.swift
//  NeobisTime
//
//  Created by Islam on 8/1/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

class RoomEventRequest: Codable {
    let events: [RoomEvent]
}
