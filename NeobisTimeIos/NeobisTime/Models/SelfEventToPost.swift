import Foundation

class SelfEventToPost: Codable {
    let summary: String
    let start: String
    let description: String
    let location = "Bishkek"
    
    init(summary: String, description: String, start: String) {
        self.summary = summary
        self.description = description
        self.start = start
    }
    
    func convertToParameters() -> [String : AnyObject] {
         let json = [
            "summary": summary,
            "start": [
                [
                    "dateTime": start,
                    "timeZone": "Asia/Bishkek"
                    
                ]
            ],
            "end": [
                [
                    "dateTime": start,
                    "timeZone": "Asia/Bishkek"
                    
                ]
            ],
            "description": description,
            "location": location
            ] as [String : AnyObject]
        return json
    }
//    func convertToParameters() -> String {
//        let jsonEncoder = JSONEncoder()
//        let jsonData = try! jsonEncoder.encode(self)
//        let json = String(data: jsonData, encoding: String.Encoding.utf16)
//        return json!
//    }
}
