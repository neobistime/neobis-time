import Foundation

class Poll: Codable {
    let id: String
    let description: String
    let owner: String
    let location: String
    let summary: String
    let choice_bool: Bool?
    let start: [DatesDecode]
    
    
    init(id: String, description: String, location: String, start: [DatesDecode], summary: String, choiseBool: Bool, owner: String) {
        
        self.id = id
        self.description = description
        self.location = location
        self.start = start
        self.summary = summary
        self.choice_bool = choiseBool
        self.owner = owner
    }
    init() {
        self.id = ""
        self.description = ""
        self.location = ""
        self.start = [DatesDecode]()
        self.summary = ""
        self.choice_bool = true
        self.owner = ""
    }
}
