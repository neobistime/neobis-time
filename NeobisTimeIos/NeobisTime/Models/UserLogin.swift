//
//  UserLogin.swift
//  NeobisTime
//
//  Created by Islam on 7/27/20.
//  Copyright © 2020 Islam. All rights reserved.
//

import Foundation

class UserLogin: Codable {
    let email: String
    let password: String
    
    func convertToParameters() -> [String : String] {
        return ["email": email, "password": password]
    }
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}
